Name - Melvin Joseph
Contact No.- 9544421846
Email id - melvinjosephn@gmail.com

jsp files used in the project:
• login.jsp – presents the login page view logic with calls to appropriate Droplets
• dashboard.jsp – presents the logged in user details like name , email address and links to jsp pages like address update, register, shopping bag.
• cart.jsp – makes appropriate calls to commerceItem droplets to iterate through all of the items within the shopping bag on the logged in user.
• wishlist.jsp – makes calls to the profileFormHandler droplet to add as well as to iterate through the existing items in the wishlist created by the user.
• productDetails.jsp – makes calls to the ProductLookup droplet to iterate through the products and display them to the user.